import asyncio
from typing import Any

import loguru
import pytest
from pytest_mock import MockerFixture


async def cancel_task(task: Any) -> None:
    try:
        task.cancel()
    except asyncio.CancelledError:
        pass


async def set_stop_event(event: asyncio.Event) -> None:
    event.set()


@pytest.fixture
def fake_loguru(mocker: MockerFixture) -> MockerFixture:
    return mocker.patch.object(loguru, "logger", autospec=True)


@pytest.fixture
def weather_data() -> dict[Any, Any]:
    return {
        "latitude": 51.1,
        "longitude": 17.039999,
        "generationtime_ms": 0.027060508728027344,
        "utc_offset_seconds": 0,
        "timezone": "GMT",
        "timezone_abbreviation": "GMT",
        "elevation": 118.0,
        "hourly_units": {"time": "iso8601", "temperature_2m": "°C", "rain": "mm"},
        "hourly": {
            "time": [
                "2023-11-11T00:00",
                "2023-11-11T01:00",
                "2023-11-11T02:00",
                "2023-11-11T03:00",
                "2023-11-11T04:00",
                "2023-11-11T05:00",
                "2023-11-11T06:00",
                "2023-11-11T07:00",
                "2023-11-11T08:00",
                "2023-11-11T09:00",
                "2023-11-11T10:00",
                "2023-11-11T11:00",
                "2023-11-11T12:00",
                "2023-11-11T13:00",
                "2023-11-11T14:00",
                "2023-11-11T15:00",
                "2023-11-11T16:00",
                "2023-11-11T17:00",
                "2023-11-11T18:00",
                "2023-11-11T19:00",
                "2023-11-11T20:00",
                "2023-11-11T21:00",
                "2023-11-11T22:00",
                "2023-11-11T23:00",
            ],
            "temperature_2m": [
                5.5,
                5.1,
                4.2,
                3.8,
                4.3,
                4.1,
                4.4,
                3.9,
                4.8,
                6.0,
                7.2,
                8.4,
                9.1,
                9.5,
                9.3,
                8.6,
                7.9,
                6.8,
                6.5,
                6.0,
                5.9,
                5.6,
                5.4,
                5.3,
            ],
            "rain": [
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                2.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                5.00,
                0.00,
                0.00,
                0.00,
                0.00,
                9.00,
                10.30,
            ],
        },
    }
