import asyncio
from dataclasses import dataclass
from typing import Any

import httpx
import pytest
from pydantic_extra_types.coordinate import Coordinate, Latitude, Longitude
from pytest_mock import MockerFixture

from tests.conftest import cancel_task, set_stop_event
from weather_anomaly_monitor.pub import WeatherFetcher


@dataclass
class Response:
    status_code: int
    text: Any
    data: dict[Any, Any]

    def json(self) -> dict[Any, Any]:
        return self.data

    def raise_for_status(self) -> None:
        if self.status_code != 200:
            raise httpx.HTTPStatusError(message="Error", response=self, request="REQUEST")


@pytest.fixture
def fake_httpx(mocker: MockerFixture) -> MockerFixture:
    return mocker.patch("weather_anomaly_monitor.pub.httpx", autospec=True)


@pytest.mark.asyncio
def describe_WeatherFetcher() -> None:
    async def it_puts_data_into_queue(fake_httpx: MockerFixture, weather_data: dict[Any, Any], fake_loguru: MockerFixture) -> None:
        fake_httpx.AsyncClient.return_value.__aenter__.return_value.get.return_value = Response(status_code=200, data=weather_data, text="TEST")
        fake_httpx.HTTPStatusError = httpx.HTTPStatusError

        test_queue: asyncio.Queue[dict[Any, Any]] = asyncio.Queue()
        stop_event = asyncio.Event()

        fetcher = WeatherFetcher(
            location=Coordinate(latitude=Latitude(90), longitude=Longitude(180)), stop_event=stop_event, queue=test_queue, check_interval=0
        )

        async with asyncio.TaskGroup() as tg:
            task1 = tg.create_task(fetcher.fetch())
            tg.create_task(cancel_task(task1))

        fake_httpx.AsyncClient.assert_called_once_with()
        fake_httpx.AsyncClient.return_value.__aenter__.return_value.get.assert_awaited_once_with(
            "https://api.open-meteo.com/v1/forecast?latitude=90.0&longitude=180.0&hourly=temperature_2m,rain&forecast_days=7"
        )
        assert not test_queue.empty()
        assert await test_queue.get() == weather_data

    async def it_logs_error_and_continues_work(fake_httpx: MockerFixture, weather_data: dict[Any, Any], fake_loguru: MockerFixture) -> None:
        fake_httpx.AsyncClient.return_value.__aenter__.return_value.get.return_value = Response(status_code=400, data=weather_data, text="ERROR")
        fake_httpx.HTTPStatusError = httpx.HTTPStatusError

        test_queue: asyncio.Queue[dict[Any, Any]] = asyncio.Queue()
        stop_event = asyncio.Event()

        fetcher = WeatherFetcher(
            location=Coordinate(latitude=Latitude(90), longitude=Longitude(180)), stop_event=stop_event, queue=test_queue, check_interval=0
        )

        async with asyncio.TaskGroup() as tg:
            task1 = tg.create_task(fetcher.fetch())
            tg.create_task(cancel_task(task1))

        fake_loguru.error.assert_called()
        assert test_queue.empty()

    async def it_ends_work_after_receiving_stop_event(fake_httpx: MockerFixture, weather_data: dict[Any, Any], fake_loguru: MockerFixture) -> None:
        fake_httpx.AsyncClient.return_value.__aenter__.return_value.get.return_value = Response(status_code=200, data=weather_data, text="TEST")
        fake_httpx.HTTPStatusError = httpx.HTTPStatusError

        test_queue: asyncio.Queue[dict[Any, Any]] = asyncio.Queue()
        stop_event = asyncio.Event()

        fetcher = WeatherFetcher(
            location=Coordinate(latitude=Latitude(90), longitude=Longitude(180)), stop_event=stop_event, queue=test_queue, check_interval=0
        )

        async with asyncio.TaskGroup() as tg:
            tg.create_task(fetcher.fetch())
            tg.create_task(set_stop_event(stop_event))

        assert fake_loguru.debug.call_count == 2
