import asyncio
from datetime import datetime
from typing import Any
from unittest.mock import call

import pytest
from pytest_mock import MockerFixture

from tests.conftest import cancel_task, set_stop_event
from weather_anomaly_monitor.models import AnomalyType
from weather_anomaly_monitor.sub import WeatherProcessor


@pytest.fixture
def weather_data() -> dict[Any, Any]:
    return {
        "latitude": 51.1,
        "longitude": 17.039999,
        "generationtime_ms": 0.027060508728027344,
        "utc_offset_seconds": 0,
        "timezone": "GMT",
        "timezone_abbreviation": "GMT",
        "elevation": 118.0,
        "hourly_units": {"time": "iso8601", "temperature_2m": "°C", "rain": "mm"},
        "hourly": {
            "time": [
                "2023-11-11T00:00",
                "2023-11-11T01:00",
                "2023-11-11T02:00",
                "2023-11-11T03:00",
                "2023-11-11T04:00",
                "2023-11-11T05:00",
                "2023-11-11T06:00",
                "2023-11-11T07:00",
                "2023-11-11T08:00",
                "2023-11-11T09:00",
                "2023-11-11T10:00",
                "2023-11-11T11:00",
                "2023-11-11T12:00",
                "2023-11-11T13:00",
                "2023-11-11T14:00",
                "2023-11-11T15:00",
                "2023-11-11T16:00",
                "2023-11-11T17:00",
                "2023-11-11T18:00",
                "2023-11-11T19:00",
                "2023-11-11T20:00",
                "2023-11-11T21:00",
                "2023-11-11T22:00",
                "2023-11-11T23:00",
            ],
            "temperature_2m": [
                5.5,
                5.1,
                4.2,
                3.8,
                4.3,
                4.1,
                4.4,
                3.9,
                4.8,
                6.0,
                7.2,
                8.4,
                9.1,
                9.5,
                9.3,
                8.6,
                7.9,
                6.8,
                6.5,
                6.0,
                5.9,
                5.6,
                5.4,
                5.3,
            ],
            "rain": [
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.00,
                0.30,
            ],
        },
    }


@pytest.fixture
def fake_weather_data_model(mocker: MockerFixture) -> MockerFixture:
    return mocker.patch("weather_anomaly_monitor.sub.WeatherData", autospec=True)


@pytest.fixture
def fake_print(mocker: MockerFixture) -> MockerFixture:
    return mocker.patch("builtins.print", autospec=True)


@pytest.mark.asyncio
def describe_WeatherProcessor() -> None:
    async def it_prints_anomaly_report(
        fake_loguru: MockerFixture, fake_weather_data_model: MockerFixture, weather_data: dict[Any, Any], fake_print: MockerFixture
    ) -> None:
        expected_temp_anomalies = ([(0, datetime.now())], [(999, datetime.now())])
        expected_rain_anomalies = [(777, datetime.now())]

        expected_message = "WARNING {location_name}, {anomaly_type}, treshold: {treshold} value: {value} on {timestamp}"

        expected_max_rain = 777
        expected_max_temp = 999
        expected_min_temp = 0

        expected_location_name = "TEST LOCATION"

        fake_weather_data_model.return_value.get_temperature_anomalies.return_value = expected_temp_anomalies
        fake_weather_data_model.return_value.get_rain_anomalies.return_value = expected_rain_anomalies

        test_queue: asyncio.Queue[dict[Any, Any]] = asyncio.Queue()
        stop_event = asyncio.Event()

        processor = WeatherProcessor(
            queue=test_queue,
            stop_event=stop_event,
            location_name=expected_location_name,
            min_temp_limit=expected_min_temp,
            max_temp_limit=expected_max_temp,
            max_rain_limit=expected_max_rain,
        )

        async with asyncio.TaskGroup() as tg:
            tg.create_task(test_queue.put(weather_data))
            task1 = tg.create_task(processor.process())
            tg.create_task(cancel_task(task1))

        assert test_queue.empty()

        fake_weather_data_model.assert_called_once_with(**weather_data["hourly"])
        fake_weather_data_model.return_value.get_rain_anomalies.assert_called_once_with(max_limit=777)
        fake_weather_data_model.return_value.get_temperature_anomalies.assert_called_once_with(max_limit=999, min_limit=0)
        fake_print.assert_has_calls(
            [
                call(
                    expected_message.format(
                        location_name=expected_location_name,
                        anomaly_type=AnomalyType.high_temp,
                        treshold=expected_max_temp,
                        value=expected_temp_anomalies[1][0][0],
                        timestamp=expected_temp_anomalies[1][0][1],
                    )
                ),
                call(
                    expected_message.format(
                        location_name=expected_location_name,
                        anomaly_type=AnomalyType.low_temp,
                        treshold=expected_min_temp,
                        value=expected_temp_anomalies[0][0][0],
                        timestamp=expected_temp_anomalies[0][0][1],
                    )
                ),
                call(
                    expected_message.format(
                        location_name=expected_location_name,
                        anomaly_type=AnomalyType.high_rain,
                        treshold=expected_max_rain,
                        value=expected_rain_anomalies[0][0],
                        timestamp=expected_rain_anomalies[0][1],
                    )
                ),
            ]
        )

    async def it_stops_work_after_receiving_stop_event(
        fake_loguru: MockerFixture, fake_weather_data_model: MockerFixture, weather_data: dict[Any, Any], fake_print: MockerFixture
    ) -> None:
        test_queue: asyncio.Queue[dict[Any, Any]] = asyncio.Queue()
        stop_event = asyncio.Event()

        expected_temp_anomalies = ([(0, datetime.now())], [(999, datetime.now())])
        expected_rain_anomalies = [(777, datetime.now())]

        fake_weather_data_model.return_value.get_temperature_anomalies.return_value = expected_temp_anomalies
        fake_weather_data_model.return_value.get_rain_anomalies.return_value = expected_rain_anomalies

        expected_max_rain = 777
        expected_max_temp = 999
        expected_min_temp = 0
        expected_location_name = "TEST LOCATION"
        processor = WeatherProcessor(
            queue=test_queue,
            stop_event=stop_event,
            location_name=expected_location_name,
            min_temp_limit=expected_min_temp,
            max_temp_limit=expected_max_temp,
            max_rain_limit=expected_max_rain,
        )

        async with asyncio.TaskGroup() as tg:
            tg.create_task(test_queue.put(weather_data))
            tg.create_task(processor.process())
            tg.create_task(set_stop_event(stop_event))
            tg.create_task(test_queue.put(weather_data))

        assert fake_loguru.debug.call_count == 2
