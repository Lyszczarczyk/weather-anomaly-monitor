import datetime
from typing import Any

from weather_anomaly_monitor.models import WeatherData


def describe_WeatherData() -> None:
    def it_returns_correct_temp_anomalies(weather_data: dict[Any, Any]) -> None:
        expected_min_anomalies = [(3.8, datetime.datetime(2023, 11, 11, 3, 0)), (3.9, datetime.datetime(2023, 11, 11, 7, 0))]
        expected_max_anomalies = [
            (9.1, datetime.datetime(2023, 11, 11, 12, 0)),
            (9.5, datetime.datetime(2023, 11, 11, 13, 0)),
            (9.3, datetime.datetime(2023, 11, 11, 14, 0)),
        ]

        model = WeatherData(**weather_data["hourly"])
        min_anomalies, max_anomalies = model.get_temperature_anomalies(min_limit=4.0, max_limit=9.0)
        assert min_anomalies == expected_min_anomalies
        assert max_anomalies == expected_max_anomalies

    def it_returns_correct_rain_anomalies(weather_data: dict[Any, Any]) -> None:
        expected_max_anomalies = [(9.0, datetime.datetime(2023, 11, 11, 22, 0)), (10.3, datetime.datetime(2023, 11, 11, 23, 0))]

        model = WeatherData(**weather_data["hourly"])
        max_anomalies = model.get_rain_anomalies(max_limit=8.0)
        assert max_anomalies == expected_max_anomalies
