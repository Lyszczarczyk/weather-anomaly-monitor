# weather-anomaly-monitor
Project for creating monitoring service that checks for temperatures and rainfall anomalies for a given treshold and location.

## Requirements
Python 3.11
## Installation
### Latest release
[Latest package release](https://gitlab.com/Lyszczarczyk/weather-anomaly-monitor/-/releases/permalink/latest)
### Poetry
To install the project using poetry tool please see: [Poetry installation guide](https://python-poetry.org/docs/#installation)
When poetry is installed:
```shell
poetry install
```
### Pip
To install the project using pip package manager use the below command:
```shell
pip install <package_name>
```
Example:
```shell
pip install weather_anomaly_monitor-0.1.0-py3-none-any.whl
```
or
```shell
pip install weather_anomaly_monitor-0.1.0.tar.gz
```
## Usage
To see the available parameters and explanation please use:
```shell
weather-anomaly-monitor --help
```
Example usage:
```shell
weather-anomaly-monitor --location-name=Warszawa --latitude=52.23 --longitude=21.01 --max-temp-limit=10.0 --min-temp-limit=5.0 --max-rain-limit=6.0
```
