import asyncio
import signal
import sys
from typing import Any, Coroutine

import loguru
import typer
from pydantic_extra_types.coordinate import Coordinate, Latitude, Longitude
from typing_extensions import Annotated

from weather_anomaly_monitor.models import LogLevel
from weather_anomaly_monitor.pub import WeatherFetcher
from weather_anomaly_monitor.sub import WeatherProcessor

app = typer.Typer()


@app.command("start")
def cli(
    latitude: Annotated[float, typer.Option(help="Latitide of the required location. Must be between -90 and 90")] = 51.10,
    longitude: Annotated[float, typer.Option(help="Longitude of the required location. Must be between -180 and 180")] = 17.03,
    location_name: Annotated[str, typer.Option(help="Name of the location")] = "Wroclaw",
    min_temp_limit: Annotated[float, typer.Option(help="Min temperature treshhold for sending anomaly alerts")] = 5.0,
    max_temp_limit: Annotated[float, typer.Option(help="Max temperature treshhold for sending anomaly alerts")] = 30.0,
    max_rain_limit: Annotated[float, typer.Option(help="Max rain treshhold for sending anomaly alerts")] = 4.0,
    check_interval: Annotated[int, typer.Option(help="How often should the weather be checked in [s]")] = 24 * 60 * 60,
    log_level: Annotated[str, typer.Option(help="Set visible logging level. Can be INFO, DEBUG, ERROR")] = LogLevel.info,
) -> None:
    loguru.logger.remove()
    loguru.logger.add(sys.stdout, level=log_level)

    asyncio.run(main(latitude, longitude, location_name, min_temp_limit, max_temp_limit, max_rain_limit, check_interval))


async def main(
    latitude: float,
    longitude: float,
    location_name: str,
    min_temp_limit: float,
    max_temp_limit: float,
    max_rain_limit: float,
    check_interval: int,
) -> None:
    data_queue: asyncio.Queue[dict[Any, Any]] = asyncio.Queue()
    stop_event: asyncio.Event = asyncio.Event()

    weather_fetcher = WeatherFetcher(
        location=Coordinate(latitude=Latitude(latitude), longitude=Longitude(longitude)),
        queue=data_queue,
        stop_event=stop_event,
        check_interval=check_interval,
    )
    weather_processor = WeatherProcessor(
        queue=data_queue,
        stop_event=stop_event,
        location_name=location_name,
        min_temp_limit=min_temp_limit,
        max_temp_limit=max_temp_limit,
        max_rain_limit=max_rain_limit,
    )
    tasks = []

    async with asyncio.TaskGroup() as tg:
        tasks.append(tg.create_task(weather_fetcher.fetch()))
        tasks.append(tg.create_task(weather_processor.process()))
        asyncio.get_running_loop().add_signal_handler(signal.SIGINT, signal_handler, stop_event, tasks)


def signal_handler(event: asyncio.Event, tasks: list[asyncio.Task[Coroutine[Any, Any, Any]]]) -> None:
    loguru.logger.info("Terminating application...")
    event.set()
    for task in tasks:
        task.cancel()
