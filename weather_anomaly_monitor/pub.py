import asyncio
import uuid
from typing import Any

import httpx
import loguru
from pydantic_extra_types.coordinate import Coordinate


class WeatherFetcher:
    _QUERY = "https://api.open-meteo.com/v1/forecast?latitude={latitude}&longitude={longitude}&hourly=temperature_2m,rain&forecast_days=7"

    def __init__(self, location: Coordinate, queue: asyncio.Queue[dict[Any, Any]], stop_event: asyncio.Event, check_interval: int):
        self._location = location
        self._queue = queue
        self._stop_event = stop_event
        self._check_interval = check_interval
        self._uuid = uuid.uuid4()

    async def fetch(self) -> None:
        loguru.logger.debug(f"Starting WeatherFetcher id: {self._uuid}")
        async with httpx.AsyncClient() as client:
            while not self._stop_event.is_set():
                response = await client.get(self._QUERY.format(latitude=self._location.latitude, longitude=self._location.longitude))
                try:
                    response.raise_for_status()
                except httpx.HTTPStatusError as error:
                    loguru.logger.error(
                        "\n Something went wrong when fetching weather data.\n STATUS CODE: {},\n MESSAGE: {}",
                        error.response.status_code,
                        error.response.text,
                    )
                    await asyncio.sleep(self._check_interval)
                    continue
                data = response.json()
                await self._queue.put(data)
                await asyncio.sleep(self._check_interval)
            loguru.logger.debug(f"Terminating WeatherFetcher id: {self._uuid}")
