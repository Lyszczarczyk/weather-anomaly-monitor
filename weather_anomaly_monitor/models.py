from datetime import datetime
from enum import StrEnum

import pydantic


class WeatherData(pydantic.BaseModel):
    time: list[datetime]
    temperature_2m: list[float]
    rain: list[float]

    def get_temperature_anomalies(self, min_limit: float, max_limit: float) -> tuple[list[tuple[float, datetime]], list[tuple[float, datetime]]]:
        min_anomalies = [(measure, timestamp) for measure, timestamp in zip(self.temperature_2m, self.time, strict=True) if measure < min_limit]
        max_anomalies = [(measure, timestamp) for measure, timestamp in zip(self.temperature_2m, self.time, strict=True) if measure > max_limit]
        return min_anomalies, max_anomalies

    def get_rain_anomalies(self, max_limit: float) -> list[tuple[float, datetime]]:
        return [(measure, timestamp) for measure, timestamp in zip(self.rain, self.time, strict=True) if measure > max_limit]


class AnomalyType(StrEnum):
    low_temp = "low temperature"
    high_temp = "high temperature"
    high_rain = "high rainfall"


class LogLevel(StrEnum):
    info = "INFO"
    debug = "DEBUG"
    error = "ERROR"
