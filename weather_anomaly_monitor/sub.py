import asyncio
import shutil
import uuid
from datetime import datetime
from typing import Any

import loguru

from weather_anomaly_monitor.models import AnomalyType, WeatherData


class WeatherProcessor:
    _BANNER = "REPORT"
    _ANOMALY_MESSAGE = "WARNING {location_name}, {anomaly_type}, treshold: {treshold} value: {value} on {timestamp}"

    def __init__(
        self,
        queue: asyncio.Queue[dict[Any, Any]],
        stop_event: asyncio.Event,
        location_name: str,
        min_temp_limit: float,
        max_temp_limit: float,
        max_rain_limit: float,
    ):
        self._queue = queue
        self._stop_event = stop_event
        self._location_name = location_name
        self._min_temp_limit = min_temp_limit
        self._max_temp_limit = max_temp_limit
        self._max_rain_limit = max_rain_limit
        self._uuid = uuid.uuid4()

    async def process(self) -> None:
        loguru.logger.debug(f"Starting WeatherProcessor id: {self._uuid}")
        print(f"{self._BANNER}".center(shutil.get_terminal_size().columns, "-"))

        while not self._stop_event.is_set():
            raw_data = await self._queue.get()
            data = WeatherData(**raw_data["hourly"])
            min_temp_anomalies, max_temp_anomalies = data.get_temperature_anomalies(min_limit=self._min_temp_limit, max_limit=self._max_temp_limit)
            max_rain_anomalies = data.get_rain_anomalies(max_limit=self._max_rain_limit)
            self._prepare_high_temp_report(anomalies=max_temp_anomalies)
            self._prepare_low_temp_report(anomalies=min_temp_anomalies)
            self._prepare_high_rain_report(anomalies=max_rain_anomalies)
        loguru.logger.debug(f"Terminating WeatherProcessor id: {self._uuid}")

    def _prepare_low_temp_report(self, anomalies: list[tuple[float, datetime]]) -> None:
        for anomaly in anomalies:
            print(
                self._ANOMALY_MESSAGE.format(
                    location_name=self._location_name,
                    anomaly_type=AnomalyType.low_temp,
                    treshold=self._min_temp_limit,
                    value=anomaly[0],
                    timestamp=anomaly[1],
                )
            )

    def _prepare_high_temp_report(self, anomalies: list[tuple[float, datetime]]) -> None:
        for anomaly in anomalies:
            print(
                self._ANOMALY_MESSAGE.format(
                    location_name=self._location_name,
                    anomaly_type=AnomalyType.high_temp,
                    treshold=self._max_temp_limit,
                    value=anomaly[0],
                    timestamp=anomaly[1],
                )
            )

    def _prepare_high_rain_report(self, anomalies: list[tuple[float, datetime]]) -> None:
        for anomaly in anomalies:
            print(
                self._ANOMALY_MESSAGE.format(
                    location_name=self._location_name,
                    anomaly_type=AnomalyType.high_rain,
                    treshold=self._max_rain_limit,
                    value=anomaly[0],
                    timestamp=anomaly[1],
                )
            )
